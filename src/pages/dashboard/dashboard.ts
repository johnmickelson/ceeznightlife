import { Component, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { NewEventPage } from '../new-event/new-event';
import { MenuPage } from '../menu/menu';
import { DateTime } from 'ionic-angular';
import { SharedModule } from '../../shared.module';
import { EventsPage } from '../events/events';
import { EventDetailPage } from '../event-detail/event-detail';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage extends BasePage{

  title: string = "Dashboard";
  next_event: any;
  next_host_event: any;
  cloud_url = SharedModule.CLOUD_URL;

  constructor(public injector: Injector) {
    super(injector);

    this.showLoadingView();
    let query = {
      where: {
        date: {
          gte: 1521435077947
        }
      },
      order: "date ASC",
      limit: 1
    };
    let param = encodeURI(JSON.stringify(query));
    this.server.getResult("Partygoers/" + this.userData.getUserInfo() + "/outings?filter=" + param, this.userData.getAccessToken())
      .subscribe((data) => {
        this.showContentView();
        if(data.length > 0) {
          this.next_host_event =data[0];
        }
      }, (err) => {
        this.showContentView();
    });

    this.server.getResult("Partygoers/" + this.userData.getUserInfo() + "/reservations?filter=" + param, this.userData.getAccessToken())
      .subscribe((data) => {
        if(data.length > 0) {
          let event = data[0];
          console.log(event);

          this.server.getResult("Outings?filter=" + JSON.stringify({
            where: {
              id: event['outingId']
            },
            include: "owner"
          })).subscribe((resp) => {
            this.next_event = resp[0];
          });

        }
      }, (err) => {
    });
  }

  createNewEvent() {
    this.navigateTo(NewEventPage);
  }

  gotoEvent(id) {
    this.navCtrl.push(EventDetailPage, id); 
  }

  attendEvents() {
    this.navCtrl.push(EventsPage);
  }

  gotoMenu() {
    this.navCtrl.push(MenuPage);
  }

}
