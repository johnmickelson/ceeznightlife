import { Component, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { MenuPage } from '../menu/menu';
import { EventDetailPage } from '../event-detail/event-detail';
import { SharedModule } from '../../shared.module';

/**
 * Generated class for the MyTicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-tickets',
  templateUrl: 'my-tickets.html',
})
export class MyTicketsPage extends BasePage{

  title: string = "My Tickets";
  events: Array<{
    id: string,
    src: string,
    title: string,
    date: string,
    venue: string,
    address: string,
  }> = [];
  cloud_url = SharedModule.CLOUD_URL;

  constructor(public injector: Injector) {
    super(injector);
  }

  ionViewDidEnter() {
    this.loadTickets();
  }

  gotoMenu() {
    this.navCtrl.push(MenuPage);
  }

  gotoEvent(id) {
    this.navCtrl.push(EventDetailPage, id);
  }

  loadTickets() {
    this.showLoadingView();


    let accessToken = this.userData.getAccessToken();
    this.server.getResult("RSVPs?filter=" + encodeURI(JSON.stringify({
      where: {
        attendeeId: this.userData.getUserInfo()
      },
      order: "date ASC"
    })), accessToken).subscribe((data) => {
      this.showContentView();
      if(data.length > 0) {

        let outingArray = [];
        data.forEach((item) => {
          outingArray.push(item.outingId);
        })

        let param = {
          where: {
            id: {
              inq: outingArray
            }
          },
          order: "date ASC"
        };

        console.log(param);

        this.server.getResult("Outings?filter=" + encodeURI(JSON.stringify(param)), accessToken)
          .subscribe((resp) => {
            this.events = resp;
          });
      }
    }, (err) => {
      this.showContentView();
    });
  }

}
